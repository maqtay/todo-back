# Todo App Backend with Golang

Golang echo ile yazılmış **todo** projesi. Bu repo projenin backend kısmını içerir. [front.](https://gitlab.com/maqtay/todo-front)

Canlı adresi: 
http://159.89.10.62/

## Kullanılan teknolojiler

ToDo eklemek, eklenen todo'ları listelemek ve silmek.

### Proje Kurulumu
```
$ go mod download
$ docker build -t maktay/todoback
$ docker run maktay/todoback
```

### Deployment
```
$ docker stack deploy --compose-file docker-compose.yml todo-app
```
